// Taylor Rongaus and Ronni Sardina
// 2/15/16
// cdl_list.h- In Class Code

#ifndef CDL_LIST
#define CDL_LIST
#include <iostream>
#include <deque>
#include "node.h"
using namespace std;

template<typename T>
class cdl_list {
	public:
		cdl_list();								// default constructor
		~cdl_list();								// deconstructor
		void insertFromFront(T&);		// inserts an element of type T in the front
		void insertFromBack(T&);		// inserts an element of type T at the back
		void deleteFront();					// delete front element
		void deleteBack();					// delete back element
		void reset();							// returns to the front of the list
		void next();							// moves to the next element of the list
		T currentValue();						// returns current value at the top of the list
		void deleteElement();				// deletes an element from the list
		operator++();						// runs next
	private:
		deque<T>myList;
		int position;							// used for iteration
		int current;							// indicates the node currently visited by iterator
}

#endif

