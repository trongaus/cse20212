// Taylor Rongaus and Ronni Sardina
// 2/15/16
// cdl_list.cpp- In Class Code

#include "cdl_list.h"

// default constructor
cdl_list::cdl_list() {

}

// deconstructor
cdl_list::~cdl_list(){
	myList.clear();											// empty the list
}

// function implementation
template<typename T>
void cdl_list<T>::insertFromFront(T &value) {		// inserts element of type T in the front
	myList.push_front(value);
}

template<typename T>
void cdl_list<T>::insertFromBack(T &value) {		// inserts element of type T at the back
	myList.push_back(value);
}

template<typename T>
void cdl_list<T>::deleteFront() {					// delete front element if list isn't empty
	if (!myList.empty())
		myList.pop_front();
}

template<typename T>
void cdl_list<T>::deleteBack() {				// delete back element if list isn't empty
	if (!myList.empty())
		myList.pop_back();
}

template<typename T>
void cdl_list<T>::reset() {						// returns to the front of the list
	position = 0;
}

template<typename T>
void cdl_list<T>::next() {							// moves to the next element of the list
	position++;
	if (position == myList.size())
		position = 0;
}

template<typename T>
T cdl_list<T>::currentValue() {					// returns current value at the top of the list
	return (myList[position]);
}

template<typename T>
void cdl_list<T>::deleteElement() { 			// deletes an element from the list
	if (!myList.empty()) {
		myList.erase(myList.begin() + position, 1);
		if (position == myList.size())
			position = 0;
	}
}

