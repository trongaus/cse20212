// C4Board.cpp (Implementation file)
// 1/18/16
// Author: Taylor Rongaus

#include "C4Board.h"

using namespace std;

// default constructor for the C4Col class
C4Board::C4Board() {
	Board = new C4Col[10];
	numCol = 7;				// default number of columns for the board
} 

 // deconstructor
C4Board::~C4Board() {
	delete [] Board;
} 

// implementation of member functions

void C4Board::display() {			// display current board in simple text
	// print number header
	cout << " 1 2 3 4 5 6 7" << endl;
	// print game board frame
	for (int i = 1; i < Board[i].getMaxDiscs()+1; i++) {
		cout << "|";
		for (int j = 0; j < numCol; j++) {
			cout << Board[j].getDisc(i) << "|";
		}
		cout << endl;
	}	
}

void C4Board::play() {				// allow two players to play a game

	// display initial board
	display();
	char disc;						// initialize disc
	int turn = 0;					// initialize turns
	int colchoice = 1;				// initialize column choice
	
	// run game until winner
	while (1) {

		// check for a win
		if (helper() == 1) {
			if (((turn%2)+1) == 1) {
				cout << "Congrats! Player 2 wins!" << endl;
				return;
			}
			else {
				cout << "Congrats! Player 1 wins!" << endl;
				return;
			}
		}
	
		// determine whose turn it is
		if (((turn%2)+1) == 1) {
			// set player's default disc character
			disc = 'X';	
			// ask player for column and determine if it's valid
			cout << "Player 1's turn. To which column would you like to add a disc?" << endl;
			cin >> colchoice;
			cout << "__________________" << endl << endl;
			while ((colchoice>7) || (colchoice<1)) {
				// exit game if -1 pressed
				if (colchoice == -1) return;
				cout << "Invalid column choice. Please try again." << endl;
				cin >> colchoice;
			}
		}

		else {
			// set player's default disc character
			disc = 'O';	
			// ask player for column and determine if it's valid
			cout << "Player 2's turn. To which column would you like to add a disc?" << endl;
			cin >> colchoice;
			cout << "__________________" << endl << endl;
			cout << endl;
			while ((colchoice>7) || (colchoice<1)) {
				// exit game if -1 pressed
				if (colchoice == -1) return;
				cout << "Invalid column choice. Please try again." << endl;
				cin >> colchoice;
			}
		}
		
		// update and display board
		Board[colchoice-1].addDisc(disc);
		display();
		// switch player
		turn++;
	}
}

int C4Board::helper() {				// determines if player has won
	int i = 0, j = 0;				// initialize counters
	// loop through every element of the board
	for (i=1;i<=Board[i].getDisc(j)+7;i++) {
		// seg fault if disc placed in col 1??
		for (j=1;j<=numCol;j++) {
			// determine if any disc is in the current position
			if (Board[i].getDisc(j) != ' ') {
				// check for horizontal win
				if (((Board[i].getDisc(j)) == (Board[i+1].getDisc(j))) && ((Board[i].getDisc(j)) == (Board[i+2].getDisc(j))) && ((Board[i].getDisc(j)) == (Board[i+3].getDisc(j)))) {
					return 1;
				}
				// check for vertical win
				if (((Board[i].getDisc(j)) == (Board[i].getDisc(j+1))) && ((Board[i].getDisc(j)) == (Board[i].getDisc(j+2))) && ((Board[i].getDisc(j)) == (Board[i].getDisc(j+3)))) {
					return 1;
				}
				// check for \ diagonal win
				if (((Board[i].getDisc(j)) == (Board[i+1].getDisc(j+1))) && ((Board[i].getDisc(j)) == (Board[i+2].getDisc(j+2))) && ((Board[i].getDisc(j)) == (Board[i+3].getDisc(j+3)))) {
					return 1;
				}
				// check for / diagonal win
				if (((Board[i].getDisc(j)) == (Board[i-1].getDisc(j+1))) && ((Board[i].getDisc(j)) == (Board[i-2].getDisc(j+2))) && ((Board[i].getDisc(j)) == (Board[i-3].getDisc(j+3)))) {
					return 1;
				}
			}
		}
	}
}

