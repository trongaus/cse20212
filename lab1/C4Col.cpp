// C4Col.cpp (Implementation file)
// 1/18/16
// Author: Taylor Rongaus

#include "C4Col.h"	// include necessary class definitions

using namespace std;

// default constructor for the C4Col class
C4Col::C4Col() {
	// initialize current and max # of discs
	numdiscs = 0;
	maxdiscs = 6;
	// initialize character array with ' ' characters
	discs = new char[10];
	for (int i = 1;i<=7; i++) {
		discs[i] = ' ';
	}
} 

 // deconstructor
C4Col::~C4Col() {
	delete [] discs;
} 

// implementation of member functions
int C4Col::isFull() {		// determines if a column is full
	if (numdiscs == maxdiscs) {
		cout << "The column is full. Please try again." << endl;
		return 1;			// return 1 if column is full
	}
	else return 0;				// return 0 is not
}

char C4Col::getDisc(int i) {	// returns the requested element of the discs array
	return discs[i];	
}
		
int C4Col::getMaxDiscs() {		// returns the max # of discs
	return maxdiscs;
}		
		
void C4Col::addDisc(char disc) {	// adds the char representing a disc to the next open slot in the disc array
	// check if the column is full
	if (isFull());
	// add disk to column if not
	for (int i = (maxdiscs); i>=1; i--) {
		if (discs[i] == ' ') {
			discs[i] = disc;
			numdiscs++;	
			return;	
		}
	}
}
