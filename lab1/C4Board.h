// C4Board.h (Class definition)
// 1/18/16
// Author:Taylor Rongaus

#include <iostream>
#include <iomanip>
#include "C4Col.h"

using namespace std;
// C4Board class definition
class C4Board {
	public: 
		C4Board();				// default constructor
		~C4Board();				// deconstructor
		void display();			// display current board in simple text
		void play();			// allow two players to play a game
	private:
		int numCol;				// number of columns
		int helper();			// helper function
		C4Col *Board;			// array of C4Col objects to represent the board
};
