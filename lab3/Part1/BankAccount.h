// BankAccount.h (class definition)
// Due 2/8/16
// Author: Taylor Rongaus
// Base Class

#ifndef BANKACCOUNT_H
#define BANKACCOUNT_H
#include <iostream>
#include <iomanip>
#include <string>
#include "math.h"
using namespace std;

class BankAccount {
	public:
		BankAccount();					// default constructor
	private:
		string name;
		string SSN;
		string address;
};

#endif
