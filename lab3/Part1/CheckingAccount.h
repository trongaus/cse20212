// CheckingAccount.h (class definition)
// Due 2/8/16
// Author: Taylor Rongaus
// Derived class of BankAccount

#ifndef CHECKINGACCOUNT_H
#define CHECKINGACCOUNT_H
#include <iostream>
#include <iomanip>
#include <string>
#include "math.h"
#include "BankAccount.h"
using namespace std;

// Derived Undergrad class; inherits from Student
class CheckingAccount : public BankAccount {
	public:
		CheckingAccount();		// default constructor for CheckingAccount class
		void print();			// fun print function
	private:
		double loot;
		int numCannons;
};

#endif
