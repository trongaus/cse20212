#include "BankAccount.h"
#include "CheckingAccount.h"
#include "SavingsAccount.h"

using namespace std;

int main() {
	BankAccount b; 			// instantiate an object of each class
	SavingsAccount s;
	CheckingAccount c;
	
	s.print();				// call print functions
	c.print();
}
