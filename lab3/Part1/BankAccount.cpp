// BankAccount.cpp
// Due 2/8/16
// Author: Taylor Rongaus
// Base Class

#include "BankAccount.h"

using namespace std;

// default constructor for the BankAccount class
BankAccount::BankAccount() {
	name = "Patchy";
	SSN = "123456789";
	address = "The middle of the ocean";
}
