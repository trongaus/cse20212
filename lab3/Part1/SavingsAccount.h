// SavingsAccount.h (class definition)
// Due 2/8/16
// Author: Taylor Rongaus
// Derived class of BankAccount

#ifndef SAVINGSACCOUNT_H
#define SAVINGSACCOUNT_H
#include <iostream>
#include <iomanip>
#include <string>
#include "math.h"
#include "BankAccount.h"
using namespace std;

// Derived Undergrad class; inherits from Student
class SavingsAccount : public BankAccount {
	public:
		SavingsAccount();		// default constructor for SavingsAccount class
		void print();			// fun print function
	private:
		double buriedTreasure;
		int numRum;
};

#endif


