// CheckingAccount.cpp
// Due 2/8/16
// Author: Taylor Rongaus
// Derived Class of BankAccount

#include "BankAccount.h"
#include "CheckingAccount.h"

using namespace std;

// default constructor for the CheckingAccount class
CheckingAccount::CheckingAccount() {
	loot = 200;
	numCannons = 10;
}

// derived class function calls

void CheckingAccount::print() {
	cout << "FIRE the " << numCannons << " cannons, matey!" << endl;
	cout << loot << " gold doubloons? That's enough to buy you a new parrot." << endl;
}

