#include "Package.h"
#include "TwoDayPackage.h"
#include "OvernightPackage.h"
#include <iomanip>

using namespace std;

int main() {

	TwoDayPackage t ("Taylor","46556",5,7,10);
	OvernightPackage o ("Jordan","15241",7,12,20);
	
	cout << fixed << setprecision(2) << "The cost of shipping a two day package is: $" << t.CalculateCost() << endl;
	cout << fixed << setprecision(2) << "The cost of shipping an overnight package is: $" << o.CalculateCost() << endl;

}
