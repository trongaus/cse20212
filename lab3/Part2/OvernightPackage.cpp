// OvernightPackage.cpp
// Due 2/8/16
// Author: Taylor Rongaus
// Derived Class of Package

#include "Package.h"
#include "OvernightPackage.h"

using namespace std;

// default constructor for the OvernightPackage class
OvernightPackage::OvernightPackage(string n, string z, double w, double s, double e) : Package(n,z,w,s) {

	extraCostPerOunce = e;

}

// derived class function calls

// returns cost to ship package
double OvernightPackage::CalculateCost() {

	return (Package::CalculateCost() + extraCostPerOunce*getWeight());

}
