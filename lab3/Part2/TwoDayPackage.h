// TwoDayPackage.h (class definition)
// Due 2/8/16
// Author: Taylor Rongaus
// Derived class of Package

#ifndef TWODAYPACKAGE_H
#define TWODAYPACKAGE_H
#include <iostream>
#include <iomanip>
#include <string>
#include "math.h"
#include "Package.h"
using namespace std;

// Derived TwoDayPackage class; inherits from Package
class TwoDayPackage : public Package {
	public:
		TwoDayPackage(string, string, double, double, double);	// non-default constructor for TwoDayPackage class
		double CalculateCost();		// returns cost to ship package
	private:
		double additionalCost;	// additional expedited shipping cost
};

#endif
