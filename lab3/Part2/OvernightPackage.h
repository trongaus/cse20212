// OvernightPackage.h (class definition)
// Due 2/8/16
// Author: Taylor Rongaus
// Derived class of Package

#ifndef OVERNIGHTPACKAGE_H
#define OVERNIGHTPACKAGE_H
#include <iostream>
#include <iomanip>
#include <string>
#include "math.h"
#include "Package.h"
using namespace std;

// Derived OvernightPackage class; inherits from Package
class OvernightPackage : public Package {
	public:
		OvernightPackage(string, string, double, double, double);		// default constructor for OvernightPackage class
		double CalculateCost();	// returns cost to ship package
	private:
		double extraCostPerOunce; // extra fee per ounce for overnight delivery
};

#endif
