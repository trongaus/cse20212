// TwoDayPackage.cpp
// Due 2/8/16
// Author: Taylor Rongaus
// Derived Class of Package

#include "Package.h"
#include "TwoDayPackage.h"

using namespace std;

// default constructor for the TwoDayPackage class
TwoDayPackage::TwoDayPackage(string n, string z, double w, double s, double a) : Package(n,z,w,s) {
	
	additionalCost = a;

}

// derived class function calls

// returns cost to ship package
double TwoDayPackage::CalculateCost() {
	return (Package::CalculateCost() + additionalCost);
}
