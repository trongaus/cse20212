// Package.cpp
// Due 2/8/16
// Author: Taylor Rongaus
// Base Class

#include "Package.h"

using namespace std;

// non-default constructor
Package::Package(string n, string z, double w, double s) {
	
	name = n;
	zip = z;
	weight = w;
	shipCost = s;
	
	if (w < 0) {
		cout << "Error: Weight cannot be less than zero." << endl;
	}
	if (s < 0) {
		cout << "Error: Shipping cost cannot be less than zero." << endl;
	}
}

// base class function calls

// returns cost to ship package
double Package::CalculateCost() {
	return (shipCost*weight);
}

// get functions
string Package::getName(){
	return name;
}

string Package::getZip(){
	return zip;
}

double Package::getWeight(){
	return weight;
}


