#include "Package.h"
#include "TwoDayPackage.h"
#include "OvernightPackage.h"
#include <vector>

using namespace std;

int main() {

	// initialize cost of sending all of the packages
	double totalCost = 0;
	
	// random address info/ package costs
	TwoDayPackage P0 ("Abby","12345",2,3,4);
	TwoDayPackage P1 ("Bob","90210",5,6,7);
	TwoDayPackage P2 ("Carly","86573",8,9,10);
	OvernightPackage P3 ("Daniel","33333",11,12,13);
	OvernightPackage P4 ("Emily","01010",14,15,16);
	OvernightPackage P5 ("Fred","46556",17,18,19);
	
	// container of derived class pointers
	vector< Package* > packages(6);
	packages[0] = &P0;
	packages[1] = &P1;
	packages[2] = &P2;
	packages[3] = &P3;
	packages[4] = &P4;
	packages[5] = &P5;
	
	// print each person's info & increment totalCost
	for (int i = 0; i < 6; i++) {
		packages[i] -> print();
		totalCost += packages[i]->CalculateCost();
		cout << endl;
	}
	
	// print total cost of sending packages
	cout << "Total cost of sending all of the packages: $" << totalCost << endl;
	
}
