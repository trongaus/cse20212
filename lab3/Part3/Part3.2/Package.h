// Package.h (class definition)
// Due 2/8/16
// Author: Taylor Rongaus
// Base Class

#ifndef PACKAGE_H
#define PACKAGE_H
#include <iostream>
#include <iomanip>
#include <string>
#include "math.h"
using namespace std;

class Package {
	public:
		Package(string, string, double, double);	// non-default constructor
		double virtual CalculateCost();		// returns cost to ship package
		void virtual print() = 0;			// pure virtual print function
		string getName();
		string getZip();
		double getWeight();
	private:
		string name;
		string zip;
		double weight;
		double shipCost;
};

#endif
