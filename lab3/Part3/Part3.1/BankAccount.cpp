// BankAccount.cpp
// Due 2/8/16
// Author: Taylor Rongaus
// Base Class

#include "BankAccount.h"

using namespace std;

// default constructor for the BankAccount class
BankAccount::BankAccount() {
	name = "Jack";
	SSN = "123456789";
	address = "Tortuga";
}

// non-default constructor for the BankAccount class
BankAccount::BankAccount(string n, string s, string a) {
	name = n;
	SSN = s;
	address = a;
}

// set functions
void BankAccount::setName(string n) {
	name = n;
}

void BankAccount::setSSN(string s) {
	SSN = s;
}


void BankAccount::setAddress(string a) {
	address = a;
}

// get functions
string BankAccount::getName() {
	return name;
}

string BankAccount::getSSN() {
	return SSN;
}

string BankAccount::getAddress() {
	return address;
}

