// SavingsAccount.cpp
// Due 2/8/16
// Author: Taylor Rongaus
// Derived Class of BankAccount

#include "BankAccount.h"
#include "SavingsAccount.h"

using namespace std;

// default constructor for the SavingsAccount class
SavingsAccount::SavingsAccount() {
	numRum = 42;
	buriedTreasure = 1.42;
}

// derived class function calls

void SavingsAccount::print() {
	cout << "Aye! Ye only have " << numRum << " barrels of rum left! Why has the rum gone?" << endl;
	cout << "And we're down to " << buriedTreasure << " million in gold?! Stop wasting our gold on rum!" << endl;
}

