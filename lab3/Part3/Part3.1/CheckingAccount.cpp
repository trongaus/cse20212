// CheckingAccount.cpp
// Due 2/8/16
// Author: Taylor Rongaus
// Derived Class of BankAccount

#include "BankAccount.h"
#include "CheckingAccount.h"

using namespace std;

// non-default constructor for the CheckingAccount class
CheckingAccount::CheckingAccount(double l, int nc, string n, string s, string a) {
	loot = l;
	numCannons = nc;
	myAccount.setName(n);
	myAccount.setSSN(s);
	myAccount.setAddress(a);
}

// derived class function calls
void CheckingAccount::print() {
	cout << "Are you ready kids? AYE AYE, " << myAccount.getName() << endl;
	cout << "Fire the " << numCannons << " cannons, matey!" << endl;
}

