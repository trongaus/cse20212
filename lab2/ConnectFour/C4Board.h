// C4Board.h (Class definition)
// REVAMPED
// Due 1/31/16
// Author:Taylor Rongaus

#include <iostream>
#include <iomanip>
#include "C4Col.h"

using namespace std;

// C4Board class definition
class C4Board {
	friend ostream &operator << (ostream &output, C4Board &B);
	public: 
		C4Board();				// default constructor
		void play();			// allow two players to play a game
	private:
		int numCol;				// number of columns
		int helper();			// helper function
		C4Col Board[7];			// array of C4Col objects to represent the board
};
