// main.cpp (Driver program)
// Due 1/31/16
// Author: Taylor Rongaus

#include "C4Board.h"		// class definition for C4Board used below

using namespace std;

// begin main execution
int main() {
	C4Board c4;			// instantiate an instance of a C4Board object
	c4.play();			// play the game
}
