// C4Col.h (Class definition)
// 1/18/16
// Author:Taylor Rongaus

#include <iostream>
#include <iomanip>

using namespace std;

// C4Col class definition
class C4Col {
	public: 
		C4Col();			// default constructor
		C4Col operator+=(char);
		int isFull();		// determines if a column is full
		char getDisc(int);	// returns the requested element of the discs array
		int getMaxDiscs();	// returns the max # of discs
		void addDisc(char);	// adds the char representing a disc to the next open slot in the disc array
	private:
		int numdiscs;		// stores # of discs placed in column
		int maxdiscs;		// stores max # of discs allowed
		char discs[10];		// stores representations of each players' discs
};
