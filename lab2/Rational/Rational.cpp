// Rational.cpp
// Due 1/31/16
// Author: Taylor Rongaus

#include "Rational.h"

using namespace std;

// default constructor for the Rational class
Rational::Rational() {
	numer = 1;
	denom = 1;
}

// non-default constructor for the Rational class
Rational::Rational(int n,int d) {
	numer = n;
	denom = d;
	// display error message if denominator is zero
	if (denom == 0) {
		cout << "Error: denominator cannot be equal to zero. Denominator will be set to one." << endl;
		denom = 1;
		return;
	}
	reduce();
}

// implementation of member functions

// function to reduce the fraction
void Rational::reduce() {
	// reduce to 1/1
	if (numer == denom) {
		numer = 1;
		denom = 1;
	}
	// reduce fraction
	for (int i=2; i<numer || i<denom; i++) {
		while ((numer%i == 0) && (denom%i == 0)) {
			numer = numer/i;
			denom = denom/i;
		}
	}
}

// overload the << operator
ostream &operator<<(ostream &output, Rational &r) {
	output << r.numer << "/" << r.denom;
	return output;
}

// overload the + operator
Rational &Rational::operator+(Rational &r) {
	int newNumer, newDenom; 
	newNumer = numer * r.denom + r.numer * denom;
	newDenom = denom * r.denom;
	Rational ratAdd(newNumer,newDenom);
	return ratAdd;
}

// overload the - operator
Rational &Rational::operator-(Rational &r) {
	int newNumer, newDenom; 
	newNumer = numer * r.denom - r.numer * denom;
	newDenom = denom * r.denom;
	Rational ratSub(newNumer,newDenom);
	return ratSub;
}

// overload the * operator
Rational &Rational::operator*(Rational &r) {
	int newNumer, newDenom; 
	newNumer = numer * r.numer;
	newDenom = denom * r.denom;
	Rational ratMult(newNumer,newDenom);
	return ratMult;
}

// overload the ^ operator
Rational &Rational::operator^(int power) {
	int newNumer, newDenom; 
	newNumer = pow(numer,power);
	newDenom = pow(denom,power);
	Rational ratPow(newNumer,newDenom);
	return ratPow;
}

