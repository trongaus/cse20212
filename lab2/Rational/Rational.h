// Rational.h (class definition)
// Due 1/31/16
// Author: Taylor Rongaus

#include <iostream>
#include <iomanip>
#include "math.h"

using namespace std;

// Rational class definition

class Rational {
	friend ostream &operator<<(ostream &output, Rational &r);
	public:
		Rational();							// default constructor
		Rational(int,int);					// non-default constructor
		Rational &operator+(Rational &);	// overloaded operators
		Rational &operator-(Rational &);
		Rational &operator*(Rational &);
		Rational &operator^(int);
	private:
		int numer;
		int denom;
		void reduce();			// reduce the rational fraction generated
};
