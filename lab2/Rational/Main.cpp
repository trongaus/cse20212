#include "Rational.h" 

using namespace std;

int main () {

	int numer1,numer2,denom1,denom2;

	// get value of numer1 and denom1 from user
	cout << endl;
	cout << "Please enter a value for the 1st numerator: ";
	cin >> numer1;
	cout << "Please enter a value for the 1st denominator: ";
	cin >> denom1;
	
	Rational rat1(numer1,denom1);	// invoke non-default constructor
	
	// get value of numer2 and denom2 from user
	cout << "Please enter a value for the 2nd numerator: ";
	cin >> numer2;
	cout << "Please enter a value for the 2nd denominator: ";
	cin >> denom2;

	Rational rat2(numer2,denom2);	// invoke non-default constructor
	
	// Get a exponential value from the user
	int power;
	cout << "Please enter an integer exponent from 1-9: ";
	cin >> power;
	cout << endl;
	
	cout << "First number: " << rat1 << endl;
	cout << "Second number: " << rat2 << endl << endl;
	cout << "Addition: \t\t\t" << rat1 + rat2 << endl;
	cout << "Subtraction: \t\t\t" << rat1 - rat2 << endl;
	cout << "Multiplication: \t\t" << rat1 * rat2 << endl;
	cout << "Exponential of 1st number: \t" << (rat1^power) << endl;
	cout << "Exponential of 2nd number: \t" << (rat2^power) << endl << endl;
	
	
}
