#include "CardDeck.h"
using namespace std;

int main() {
	CardDeck cd (52);				// instantiate an object of the CardDeck class, 52 cards
	srand(time(NULL));				// necessary for random_shuffle to be truly random
	
	// initialize the deck by shuffling the cards
	cd.shuffle();
	// run Blackjack simulation
	cd.playBlackjack();
	
	
}
