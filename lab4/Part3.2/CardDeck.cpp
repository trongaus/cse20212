// CardDeck.cpp
// Due 2/15/16
// Author: Taylor Rongaus
// Base Class

#include "CardDeck.h"

// non-default constructor
CardDeck::CardDeck(int n) {
	n = n;
	// place integers into deck
	for (int i = 1; i <= n; i++) {
		deck.push_back(i);
	}	
	
}

// implement member functions

// function to return size of deck
int CardDeck::getSize() {
	return deck.size();
}

// function to shuffle deck
void CardDeck::shuffle() {
	random_shuffle(deck.begin(),deck.end());
}

// function that returns whether deck is in order
int CardDeck::inOrder() {
	for (int i =0; i < (deck.size()-1); i++) { 
		if (deck[i] > deck[i+1]) {
			return 0;
		}
	}
	return 1;
}

// overload the << operator
ostream &operator << (ostream &output, CardDeck &C) {
	deque<int>::iterator i;
	for (i = C.deck.begin(); i != C.deck.end(); ++i) {
			if (i != ( C.deck.end() - 1)) {
				output << *i << ", ";
			}
			else {
				output << *i;
			}
	}
	output << endl;
	return output;
}

// function that displays cards in reverse order
void CardDeck::printReverse() {
	deque<int>::reverse_iterator r;
	deque<int>::const_reverse_iterator end = deck.rend();
	for (r = deck.rbegin(); r != end; ++r) {
		if (r != ( end - 1)) {
				cout << *r << ", ";
			}
			else {
				cout << *r;
			}
	}
	cout << endl;
}

// function that gets top card
int CardDeck::getTop() {
	return deck.front();
}
// function that removes top card
void CardDeck::removeTop() {
	deck.pop_front();
}

// function that deals cards to the user and dealer
int CardDeck::deal() {
	int n;
	n = (getTop()+1)%13;				// take top card, ensure it's w/in range
	removeTop();							// remove top card from the deck
	if (n == 1 | n == 11 | n == 12 | n == 0) {
		switch (n) {
			case 1:		// ace
					n = 11;
					cout << "	The card is an ace (11)" << endl;
				break;
			case 11: 	// jack
					n = 10;
					cout << "	The card is a jack (10)" << endl;
				break;
			case 12:	// queen
					n = 10;
					cout << "	The card is a queen (10)" << endl;
				break;
			case 0:	// king
					n = 10;
					cout << "	The card is a king (10)" << endl;
				break;
		}
	}
	else {
		cout << "	The card is a " << n << endl;
	}
	return n;
}

void CardDeck::playBlackjack() {
	int numPlayerWins = 0, numDealerWins = 0;
	int playerCard[10], dealerCard[10], playerSum = 0, dealerSum = 0, i=2, j=2;
	char play, playAgain = 'y';
	cout << "\n----- BLACKJACK -----" << endl;
	while (playAgain == 'y') {
		// get cards
		cout << endl;
		cout << "Your first card:" << endl; 
		playerCard[0] = deal();
		cout << "The dealer has one card." << endl;
		cout << "Your second card:" << endl; 
		playerCard[1] = deal();
		cout << "The dealer's second card:" << endl; 
		dealerCard[0] = deal();
		cout << endl;

		// calculate/ display initial sums
		playerSum = playerCard[0] + playerCard[1];
		dealerSum = dealerCard[0];
		cout << "You have: " << playerSum << endl;
		cout << "The dealer has: " << dealerSum << endl;
		cout << endl;
	
		// get player's move
		cout << "Would you like to hit (h) or stand (s)?	";
		cin >> play;
		cout << endl;
		while ((play == 'h') && (playerSum <= 21)) {
				cout << "Your deal: "<< endl;
				playerCard[i] = deal();
				playerSum = playerSum + playerCard[i];
				i++;
				cout << "You now have " << playerSum << endl;
				if (playerSum <= 21) {
					cout << endl;
					cout << "Would you like to hit (h) or stand (s)? ";
					cin >> play;
				}
				else {
					play == 's';
				}	
				cout << endl;
		}
	
		// flip initial hidden dealer card
		cout << "The dealer flips his first card:" << endl;
		dealerCard[2] = deal();
		dealerSum = dealerSum + dealerCard[2];
		cout << "The dealer now has " << dealerSum << endl;
		cout << endl;
	
		// continue to hit the dealer if sum is < 17
		while ((dealerSum) < 17) {
				cout << "The dealer hits:" << endl;
				dealerCard[j] = deal();
				dealerSum = dealerSum + dealerCard[j];
				j++;
				cout << "The dealer now has " << dealerSum << endl;
				cout << endl;
		}
	
		// determine who won
		cout << endl;
		if (playerSum > dealerSum && playerSum <= 21) {
			numPlayerWins++;
			cout << "You beat the house! Play again? (y/n) ";
		}
		else if (playerSum < dealerSum && dealerSum <= 21){
			numDealerWins++;
			cout << "The house won! :( Play again? (y/n) ";
		}
		else if (playerSum > 21 && dealerSum > 21) {
			cout << "You both busted! Play again? (y/n) ";
		}
		else if (playerSum > 21 && dealerSum <= 21) {
			numDealerWins++;
			cout << "You busted! Play again? (y/n) ";
		}
		else if (playerSum <= 21 && dealerSum > 21) {
			numPlayerWins++;
			cout << "The dealer busted. You win! Play again? (y/n) ";
		}
		else if (playerSum == dealerSum) {
			cout << "You tied the dealer. Play again? (y/n) ";
		}
		cin >> playAgain;
		
		// Check the size of the deck to make sure it isn't too small
		if ((deck.size()) < 15) {
			cout << "Deck size is too small. Shuffling a new deck." << endl;
			deck.erase(deck.begin(),deck.end());		// erase all elements
			for (int k = 1; k <= 52; k++) {					// fill w/ 1-52
				deck.push_back(k);
			}
			shuffle();												// shuffle new deck
		}
		
		cout << endl;
		cout << "-----------------------------------" << endl;
		
	}
	// display number of wins if user chooses to quit
	cout << endl;
	cout << "You won " << numPlayerWins << " times." << endl;
	cout << "The dealer won " << numDealerWins << " times." << endl;
	cout << endl;
}

