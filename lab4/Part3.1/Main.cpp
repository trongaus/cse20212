#include "CardDeck.h"
using namespace std;

int main() {
	srand(time(NULL));						// necessary for random_shuffle to be truly random
	cout << endl;
	// implement "monkey sort"
	for (int i = 1; i < 4; i++) {			// repeat 3 times each
		for (int n=7; n<10; n++) {		// use 7, 8, and 9 cards
			CardDeck cd (n);				// instantiate an object of the CardDeck class
			int sortCount = 0;				// initialize counter of number of sorts
			cd.shuffle();
			while (cd.inOrder() != 1) {
					cd.shuffle();
					sortCount++;
			}
			cout << "It took " << sortCount << " shuffles to sort a deck of " << n << " cards." << endl; 
		}
		cout << endl;
	}
}
