// Taylor Rongaus
// Lab 9, due 4/3/16
// NDVector.h

#ifndef NDVECTOR
#define NDVECTOR
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
using namespace std;

template<typename T>
class NDVector {
	public:
		NDVector(int n = 10); 						// non-default constructor
		const NDVector &operator=(const NDVector<T> &);		// overloaded =
		NDVector(const NDVector<T>&);		// copy constructor
		const T &operator[](int i) const{ return array[i]; };		// overloaded []
		~NDVector();									// deconstructor
		void push_back(T value);				// add to back
		void pop_back(T);							// removes from back
		const int getSize();
		const int getCapacity();					// determines capacity of vector
		T getBack();									// access last element
		void clear();									// clears the vector
	private:
		T * array;										// pointer to 1st element
		T * temp;
		int size;											// size of filled elements of vector
		int capacity;									// size of filled/ unfilled elements
};


template<typename T>
NDVector<T>::NDVector(int n) { 						// non-default constructor
	if (n > 0) {
		array = new T [n];
		size = 0;				// stores number of filled elements of the array
		capacity = n;			// stores "size" of array including non-filled elements
	}
	else {
		cout << "Error: array size must be positive" << endl;
	}
}

template<typename T>
// overloaded operator
const NDVector<T> &NDVector <T>::operator=(const NDVector <T> &right){
	if (&right != this) {
		if (capacity != right.capacity) {
			delete [] array;
			capacity = right.capacity;
			array = new T [capacity];
		}
		for (int i = 0; i < capacity; i++) {
			array[i] = right.array[i];
		}
	}
}

template<typename T>
// copy constructor
NDVector<T>::NDVector(const NDVector<T> &vectorToCopy) : capacity(vectorToCopy.capacity){			
	array = new T[capacity];	
	for (int i = 0; i < capacity; i++) {
		array[i] = vectorToCopy.array[i];
	}
}

template<typename T>
NDVector<T>::~NDVector(){										// deconstructor
	delete [] array;
}



template<typename T>
void NDVector<T>::push_back(T value){							// add to back
	// if there's room in the array for a new element, add it at the end
	if ( size < capacity) {
		array[size] = value;
		size++;
	}
	// if there isn't room, create a new temp array and replace the old
	else {
		capacity = 2 * capacity;
		temp = new T[capacity];
		for (int i = 0; i < capacity; i++) {
			temp[i] = array[i];
		}
		delete [] array;
		array = new T[capacity];
		for (int j = 0; j < size; j ++){
			array[j] = temp[j];
		}
		delete [] temp;
		array[size] = value;
		size++;
	}
}

template<typename T>
void NDVector<T>::pop_back(T){							// removes from back
	array[size-1] = NULL;
	size--;
}

template<typename T>
const int NDVector<T>::getSize(){						// determines size of vector
	return size;
}

template<typename T>
const int NDVector<T>::getCapacity(){					// determines capacity of vector
	return capacity;
}

template<typename T>
T NDVector<T>::getBack(){									// access last element
	return array[size-1];
}

template<typename T>
void NDVector<T>::clear(){									// clears the vector
	size = 0;
}
 
#endif

