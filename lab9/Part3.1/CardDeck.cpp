// CardDeck.cpp
// Due 2/15/16
// Author: Taylor Rongaus
// Base Class

#include "CardDeck.h"
#include "NDVector.h"

// non-default constructor
CardDeck::CardDeck(int n) {
	n = n;
	// place integers into deck
	for (int i = 1; i <= n; i++) {
		deck.push_back(i);
	}	
	
}

// implement member functions

// function to return size of deck
int CardDeck::getSize() {
	return deck.getSize();
}

// function to shuffle deck
void CardDeck::shuffle() {
	srand(time(NULL));
	int swap, k;
	int *tempDeck;
	int tempSize = deck.getSize();
	tempDeck = new int[ tempSize ];
	for ( int i = 0; i < deck.getSize(); i++ ) {
		tempDeck[ i ] = deck[ i ];
	}
	for ( int j = 0; j < deck.getSize(); j++ ) {
		k = rand()%( j + 1 );
		swap = tempDeck[ k ];
		tempDeck[ k ] = tempDeck[ j ];
		tempDeck[ j ] = swap;
	}
	deck.clear();
	for ( int i = 0; i < tempSize; i++ ) {
		deck.push_back( tempDeck[ i ] );
	}
	delete[] tempDeck;
}

// function that returns whether deck is in order
int CardDeck::inOrder() {
	for (int i =0; i < (deck.getSize()-1); i++) { 
		if (deck[i] > deck[i+1]) {
			return 0;
		}
	}
	return 1;
}

// overload the << operator
ostream &operator << (ostream &output, CardDeck &C) {
	for (int i = 0; i < C.deck.getSize(); i++) {
		if (i != C.deck.getSize() - 1) {
			output << C.deck[i] << ", ";
		}
		else {
			output << C.deck[i] << endl;
		}
	}
	output << endl;
	return output;
}
