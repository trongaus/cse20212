#include "CardDeck.h"
using namespace std;

int main() {
	CardDeck cd (10);					// instantiate an object of the CardDeck class
	srand(time(NULL));			// necessary for random_shuffle to be truly random
	cout << endl;
	
	// Display the initial deck
	cout << "The deck before shuffling: " << cd; 
	
	// Determine if the deck is in order
	if (cd.inOrder() == 1) { 
		cout << "The deck is in order." << endl << endl;
	} 
	else {
		cout << "Not in order." << endl << endl;
	}
	
	// Display the shuffled deck
	cd.shuffle(); 
	cout << "The deck after shuffling: " << cd;
	
	// Determine if the deck is in order
	if (cd.inOrder() == 1) { 
		cout << "The deck is in order." << endl << endl;
	} 
	else {
		cout << "Not in order." << endl << endl;
	}

}
