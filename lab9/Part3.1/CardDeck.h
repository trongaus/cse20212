// CardDeck.h (class definition)
// Due 2/15/16
// Author: Taylor Rongaus
// Base Class

#ifndef CARDDECK_H
#define CARDDECK_H
#include "NDVector.h"
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <iomanip>
using namespace std;

class CardDeck {
	friend ostream &operator << (ostream &output, CardDeck &C);
	public:
		CardDeck(int n=52);			// non-default constructor
		int getSize();						// returns size of deck
		void shuffle();					// shuffles the deck
		int inOrder();						// determines if the cards are in order
	private:
		int n;
		NDVector <int> deck;
};


#endif

