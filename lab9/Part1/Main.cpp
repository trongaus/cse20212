// Taylor Rongaus
// Lab 9, due 4/3/16
#include "NDVector.h"
using namespace std;

int main() {
	NDVector<int> test;
	int a, b;
	for (int i = 0; i < 11; i++) {
		test.push_back(i);
	}
	test.clear();
	test.push_back(5);
	a = test.getSize();
	b= test[0];
	cout << a << endl;
	cout << b << endl;
}
