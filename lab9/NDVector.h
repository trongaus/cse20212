// Taylor Rongaus
// Lab 9, due 4/1/16
// NDVector.h

#ifndef NDVECTOR
#define NDVECTOR
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
using namespace std;

template<typename T>
class NDVector {
	public:
		NDVector(int = 10); 						// non-default constructor
		const NDVector &operator=(NDVector &);		// overloaded operator
		NDVector(const NDVector &);			// copy constructor
		~NDVector();									// deconstructor
		const T &operator[](int &);			// random access to return ith element
		void push_back(T);							// add to back
		void pop_back(T);							// removes from back
		const int getSize();						// determines size of vector
		T getBack();									// access last element
		void clear();									// clears the vector
	private:
		T * array[];										// pointer to 1st element
		int size;											// const size of vector
		int last;											// position of last element
};

// in constructor set last = 0
// whenever you set the size, set last
// whenever you add an element, increment last (be careful where you increment in relation to where you add the element-- increment, then load)
// whenever you want to increase the size/ add an element (pop/push), you need to make a temp array, delete old, make new- declare temp w/in the function so you don't have to dynamically allocate it

// 

#endif

