// Taylor Rongaus
// Lab 9, due 4/3/16
// binary.h

#ifndef BINARYTREE
#define BINARYTREE
#include "Node.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <deque>
#include <queue>
using namespace std;

template<typename T>
class BinaryTree {
	public:
		BinaryTree();						// default constructor, reads in file
	private:
		deque <T> tree;
};

template<typename T>
BinaryTree<T>::BinaryTree() {
	// read in the file
	string filename;
	cout << "Please enter a filename: ";
	cin >> filename;
	ifstream inFile;
	inFile.open(filename.c_str());
	
	int data;
	string label;
	Node<T> child;
	while (inFile.good()) {
		inFile >> data;
		inFile >> label;
		Node<T>(NULL,NULL,data,label);
		tree.push_back(child);
	}
}


#endif
