// Taylor Rongaus
// Lab 9, due 4/3/16
// node.h

#ifndef _NODE_H_
#define _NODE_H_
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
using namespace std;

template<typename T>	// templated node type
class Node {
	public:
		Node(const T &, const string &);		// constructor
		T getData() const;								// get functions
		string getLabel() const;
		Node<T>* getLeft();
		Node<T>* getRight();
		void makeParent(Node <T> *, Node<T> *);
	private:
		T data;						// data for node (i.e., "seed" where higher seed wins)
		string label;				// "name" of the node
		Node<T> *left;
		Node<T> *right;
};


// default constructor
template<typename T>
Node<T>::Node(const T &info, const string &l) : left(NULL), right(NULL), data(info), label(l){

}

// function to make a parent from the left and right node
template<typename T>
void Node<T>::makeParent(Node <T> *l, Node<T> *r) {
	right = r;
	left = l;
	if ((r -> getData()) > (l -> getData())) {
		data = r -> getData();
		label = r -> getLabel();
	}
	else {
		data = l -> getData();
		label = l -> getLabel();
	}
}

// return the data value
template<typename T>
T Node<T>::getData() const { 
	return data; 
}

// return the label of the node
template<typename T>
string Node<T>::getLabel() const{
	return label;
}

// return the left node
template<typename T>
Node<T>* Node<T>::getLeft(){
	return left;
}

// return the right node
template<typename T>
Node<T>* Node<T>::getRight(){
	return right;
}


#endif
