// Taylor Rongaus
// Lab 9, due 4/3/16
// binary.h

#ifndef BINARYTREE
#define BINARYTREE
#include "Node.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <deque>
#include <queue>
using namespace std;

template<typename T>
class BinaryTree {
	public:
		BinaryTree();								// default constructor, reads in file
		void search(T, Node<T> *);
		void inorder(Node<T> *);
		void preorder(Node<T> *);
		void postorder(Node<T> *);
	private:
		Node<T> *root;
};

template<typename T>
BinaryTree<T>::BinaryTree() {
	// read in the file to a local deque
	deque <Node<T>*> tree;
	string filename;
	cout << "Please enter a filename: ";
	cin >> filename;
	ifstream inFile;
	inFile.open(filename.c_str());
	int data;
	string label;
	while (inFile.good()) {
		inFile >> data;
		inFile >> label;
		if (inFile.eof()) break;
		Node<T> *child = new Node<T>(data, label);
		tree.push_back(child);
	}
	cout << endl;
	while (tree.size() > 1) {
		if (tree.size() == 4) {
			// print "final four" 
			cout << "FINAL FOUR: " << endl;
			for (int i = 0; i < 4; i++) {
				cout << tree[i]->getData() << " ";
				cout << tree[i]->getLabel() << endl;
			}
			cout << endl;
		}
		if (tree.size() == 2) {
			// print "championship"
			cout << "CHAMPIONSHIP: " << endl;
			cout << tree[0]->getData() << " ";
			cout << tree[0]->getLabel() << endl;
			cout << tree[1]->getData() << " ";
			cout << tree[1]->getLabel() << endl;
			cout << endl;
		}
		// create new node w/ garbage data
		Node<T> *parent = new Node<T>(0, "");
		// set data of new node
		parent -> makeParent(tree[0], tree[1]);
		// remove the two children, push parent onto back
		tree.pop_front();
		tree.pop_front();
		tree.push_back(parent);
	}
	// print "winner"
	cout << "WINNER: " << endl;
	cout << tree[0]->getData() << " ";
	cout << tree[0]->getLabel() << endl;
	// set root to last element in tree
	root = tree[0];
	// call preorder function
	cout << endl;
	cout << "PREORDER: " << endl;
	preorder(root);
	// call postorder function
	cout << endl;
	cout << "POSTORDER: " << endl;
	postorder(root);
	// call inorder function
	cout << endl;
	cout << "INORDER: " << endl;
	inorder(root);
	cout << endl;
	// call search function
	T query;
	cout << "What would you like to search for? ";
	cin >> query;
	search(query, root);
}

template<typename T>
void BinaryTree<T>::search(T query, Node<T> *r) {
	if (r != NULL) {
		if (r -> getData() == query) {
			cout << "MATCH: " << endl;
			cout << r->getData() << " " ;
			cout << r->getLabel() << endl;
		}
		else {
			if (r->getLeft() != NULL) {
				search(query, r->getLeft());
			}
			if (r->getRight() != NULL) {
				search(query, r->getRight());
			}
		}
	}
}

template<typename T>
void BinaryTree<T>::inorder(Node<T> *r) {
	if ( r != NULL ) {  
		inorder( r->getLeft() ); 						// Print items in left node
		cout << r->getData() << " ";			// Print the root items
		cout << r->getLabel() << " " << endl;
		inorder( r->getRight() );					// Print items in right node
	}
}

template<typename T>
void BinaryTree<T>::preorder(Node<T> *r) {
	if ( r != NULL ) {  
		cout << r->getData() << " ";    		// Print the root items
		cout << r->getLabel() << " " << endl;     
		preorder( r->getLeft() );   					// Print items in left node
		preorder( r->getRight() ); 					// Print items in right node
	}
}

template<typename T>
void BinaryTree<T>::postorder(Node<T> *r) {
	if ( r != NULL ) {  
		postorder( r->getLeft() );   			 	// Print items in left node
		postorder( r->getRight() );   				// Print items in right node
		cout << r->getData() << " ";      	 	// Print the root items
		cout << r->getLabel() << " " << endl;
	}
}

#endif
