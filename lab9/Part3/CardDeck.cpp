// CardDeck.cpp
// Due 2/15/16
// Author: Taylor Rongaus
// Base Class

#include "CardDeck.h"

// non-default constructor
CardDeck::CardDeck(int n) {
	n = n;
	// place integers into deck
	for (int i = 1; i <= n; i++) {
		deck.push_back(i);
	}	
	
}

// implement member functions

// function to return size of deck
int CardDeck::getSize() {
	return deck.size();
}

// function to shuffle deck
void CardDeck::shuffle() {
	
}

// function that returns whether deck is in order
int CardDeck::inOrder() {
	for (int i =0; i < (deck.size()-1); i++) { 
		if (deck[i] > deck[i+1]) {
			return 0;
		}
	}
	return 1;
}

// overload the << operator
ostream &operator << (ostream &output, CardDeck &C) {
	deque<int>::iterator i;
	for (i = C.deck.begin(); i != C.deck.end(); ++i) {
			if (i != ( C.deck.end() - 1)) {
				output << *i << ", ";
			}
			else {
				output << *i;
			}
	}
	output << endl;
	return output;
}

// function that displays cards in reverse order
void CardDeck::printReverse() {
	deque<int>::reverse_iterator r;
	deque<int>::const_reverse_iterator end = deck.rend();
	for (r = deck.rbegin(); r != end; ++r) {
		if (r != ( end - 1)) {
				cout << *r << ", ";
			}
			else {
				cout << *r;
			}
	}
	cout << endl;
}

