// mortgage.cpp (Implementation file)
// 1/18/16
// Author: Taylor Rongaus
// mortgage.cpp is a program focusing on using a class “Mortgage” for easily storing and processing multiple personal mortgages

#include <iostream>
#include <iomanip>
#include "mortgage.h"	// include class definition in mortgage.h

using namespace std;

// default constructors for the Mortgage class
Mortgage::Mortgage() {
	principal =	100000;
	payment = 500;
	error = 0;
	month = 0;
}

Mortgage::Mortgage(double pr,double r,double p) {
	// initialize variables
	principal = pr;
	payment = p;
	rate = r/100./12;
	month = error = 0;
	
	// make sure all numeric inputs are valid
	if ((principal < 0) || (rate < 0) || (payment < 0)) {
		cout << "Error: invalid input." << endl;
		error = 1;
		return;
	}
	
}


// implementation of member functions
void Mortgage::credit(double value){	// deducts value from remaining principal
	// check for error
	if (error == 1) {
		return;
	}
	principal = principal - value;
}

double Mortgage::getPrincipal(){	// returns current principal remaining
	// check for error
	if (error == 1) {
		return 0.0;
	}
	return principal;
}

void Mortgage::amortize(){		// calcs and displays amortization table
	// check for error
	if (error == 1) {
		return;
	}
	
	// print table header
	cout << "Month	Payment		Interest	Balance" << endl;

	// determine each schedule
	double interest;			// define temp interest variable
	while (principal > 0.0) { 	// ensure no negative balance
		// determine if payment is too low
		if (payment <= rate*principal) {
			cout << "Error: desired monthly payment is too low." << endl;
			return;
		}
		// perform calculations
		month++;
		interest = rate * principal;
		principal = principal + interest - payment;
		// print each line
		cout << fixed << setprecision(2) << month << "\t"<< payment << "\t\t" << interest << "\t\t" << principal << endl;
		
		// for last payment, where payment < balance
		if ((principal < payment) && (principal > 0)) {
			payment = principal;
			principal = principal - payment;
			month++;
			cout << month << "\t"<< payment << "\t\t" << rate*principal << "\t\t" << principal << endl;
			
		}
	}
	
}
