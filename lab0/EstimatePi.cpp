// EstimatePi.cpp
// 1/18/16
// Author: Taylor Rongaus
// This program will estimate pi from the infinite series π = 4 – 4/3 +4/5 - 4/7 + 4/9 – 4/11 + 4/13 

#include <iostream>

int main() {

	int n;					// number of terms entered by used
	int i;					// loop counter
	double approx = 0;			// approximation of pi
	
	// ask user for number of terms of series
	std::cout << "Enter the number of terms in the series" << std::endl;
	std::cin >> n;
	
	// ensure number is greater than 1
	while (n <= 1) {
		std::cout << "Error. Provided value must be >= 2" << std::endl;
		std::cin >> n;
	}

	// calculate pi
	for (i=1; i<=n; i++) {
		// determine if loop counter is even or odd and increment approx accordingly
		if (i%2 == 0) {
			approx = approx - (4./(2*i-1));
		}
		else {
			approx = approx + (4./(2*i-1));
		}
		// display each current estimate
		std::cout << "Estimate after " << i << " terms: " << approx << std::endl;	
	}

}
