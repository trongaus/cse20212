// mortgage.h (Class definition)
// 1/18/16
// Author:Taylor Rongaus

// mortgage class definition
class Mortgage {
	public: 
		Mortgage();				// constructors
		Mortgage(double, double, double);
		void credit(double);	// deducts value from remaining principal
		double getPrincipal();	// returns current principal remaining
		void amortize();		// calcs and displays amortization table
	private:
		double principal;		// stores the remaining principal owed
		double rate;			// stores the interest rate
		double payment;			// stores the desired monthly payment
		int month;				// stores the current month
		int error;				// check for if error occurs
};
