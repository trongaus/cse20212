// jaccard.cpp
// due: 4/18/16
// Author: Taylor Rongaus
// jaccard.cpp asks the user for the names of two text files and will compute the jaccard similarity of the two of them

#include <iomanip>
#include <iostream> 
#include <fstream>
#include <string>
#include <map>
#include <set>
#include <algorithm>
using namespace std; 

int main() {

	// read in file text file names
	// ===============================
	string filename1, filename2;
	cout << "Please enter the name of your first text file: ";
	cin >> filename1;
	cout << "Please enter the name of your second text file: ";
	cin >> filename2;
	
	// read in file 1 to set
	// ===============================
	set <string> file1;
	string tmp1;
	ifstream readfile1(filename1.c_str());
	if (readfile1.is_open()){
		while (!readfile1.eof()){
			readfile1 >> tmp1;
			// go from upper case to lower case
			transform(tmp1.begin(), tmp1.end(), tmp1.begin(), :: tolower);
			// remove punctuation
			for (int i = 0; i < tmp1.size(); i++) {
				if (ispunct(tmp1[i])) {
					tmp1.erase(i--, 1);
				}
			}
			file1.insert(tmp1);
		}
	}
	readfile1.close();
	
	// read in file 2 to set
	// ===============================
	set <string> file2;
	string tmp2;
	ifstream readfile2(filename2.c_str());
	if (readfile2.is_open()){
		while (!readfile2.eof()){
			readfile2 >> tmp2;
			// go from upper case to lower case
			transform(tmp2.begin(), tmp2.end(), tmp2.begin(), :: tolower);
			// remove punctuation
			for (int i = 0; i < tmp2.size(); i++) {
				if (ispunct(tmp2[i])) {
					tmp2.erase(i--, 1);
				}
			}
			file2.insert(tmp2);
		}
	}
	readfile2.close();

	int intersection = 0;
	// iterate through the two sets to find the intersection
	for(set<string>::iterator rr=file1.begin(); rr!=file1.end(); ++rr){
		for (set<string>::iterator ss=file2.begin(); ss!=file2.end(); ++ss){
			if (*rr == *ss){
				intersection+=1;
			}
		}
	}
	cout << "The intersection of the two sets is: " << intersection << endl;

	// compute the Jaccard similarity
	// intersection of a and b / a + b - intersection 
	double jaccard;
	double size1 = file1.size();
	double size2 = file2.size();
	jaccard = (intersection) / ((size1 + size2) - intersection);
	cout << fixed << setprecision(4) << "The Jaccard similarity is: " << jaccard << endl;

}
