// checklang.cpp 
// Author: Taylor Rongaus
// due 4/20/16
// checklang.cpp asks the user for the name of a text file and uses dictionaries in the English and Spanish languages to guess the lanaguage of the user entered text file

#include <iostream> 
#include <fstream>
#include <iomanip>
#include <string>
#include <map>
#include <set>
#include <algorithm>
using namespace std; 

int main() {

	// read in the names of the files
	// =======================
	string englishName = "english.txt";
	string spanishName = "spanish.txt";
	string filename;
	cout << "Please enter the name of your first text file: ";
	cin >> filename;

	// read in the english dictionary to a set
	// ============================
	set <string> englishDict;
	string tmp;
	ifstream englishFile(englishName.c_str());
	if (englishFile.is_open()){
		while (!englishFile.eof()){
			englishFile >> tmp;
			transform(tmp.begin(), tmp.end(), tmp.begin(), ::tolower);
			for (int i = 0; i < tmp.size(); i++){
				if (ispunct(tmp[i])){
					tmp.erase(i--, 1);
				}
			}
			englishDict.insert(tmp);
		}
	}
	englishFile.close();			

	// read in the Spanish dictionary to a set
	// ============================
	set <string> spanishDict;
	string tmp1;
	ifstream spanishFile(spanishName.c_str());
	if (spanishFile.is_open()){
		while (!spanishFile.eof()){
			spanishFile >> tmp1;
			transform(tmp1.begin(), tmp1.end(), tmp1.begin(), ::tolower);
			for (int i = 0; i < tmp1.size(); i++){
				if (ispunct(tmp1[i])){
					tmp1.erase(i--, 1);
				}
			}
			spanishDict.insert(tmp1);
		}
	}
	spanishFile.close();

	// read the text file into a map
	// =====================
	map <string, int> textMap;
	string temp;
	ifstream textFile(filename.c_str());
	if (textFile.is_open()){
		while (!textFile.eof()){
			textFile >> temp;
			transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
			for (int i = 0; i < temp.size(); i++){
				if (ispunct(temp[i])){
					temp.erase(i--, 1);
				}
			}
			textMap[temp]++;
		}
	}
	textFile.close();
			
	// iterate through the sets to determine if the test file matches the dicts
	// ===================================================
	int englishMatch = 0, spanishMatch = 0, intersection = 0;
	set <string> engMatchSet;		// set used to compute intersection 
	set <string> spanMatchSet;		// set used to compute intersection 
	// match to english dictionary 
	// ii references text file, jj references English dictionary
	for (map<string, int>::iterator ii=textMap.begin(); ii!=textMap.end(); ++ii) {
		for (set<string>::iterator jj=englishDict.begin(); jj != englishDict.end(); ++jj) {
			if ((ii->first) == (*jj)) {
				englishMatch += 1;
			}
		}
	}
	// print the number of matches between the test file and the english dict
	if (englishMatch > 1){
		cout << "There are " << englishMatch << " matches in the English dictionary." << endl;
	}
	else {
		cout << "There is " << englishMatch << " match in the English dictionary." << endl;
	}
	// kk references text file, mm references Spanish dictionary
	for (map<string, int>::iterator kk=textMap.begin(); kk!=textMap.end(); ++kk) {
		for (set<string>::iterator mm=spanishDict.begin(); mm != spanishDict.end(); ++mm) {
			if ((kk->first) == (*mm)) {
				spanishMatch += 1;
			}
		}
	}
	// print the number of matches between the test file and the spanish dict
	if (spanishMatch > 1){
		cout << "There are " << spanishMatch << " matches in the Spanish dictionary." << endl << endl;
	}
	else {
		cout << "There is " << spanishMatch << " match in the Spanish dictionary." << endl << endl;
	}

	// determine the language of the file
	if (englishMatch > spanishMatch) {
		cout << "Your file is most likely in English." << endl;
	}
	else if (englishMatch < spanishMatch) {
		cout << "Your file is most likely in Spanish." << endl;
	}
	else {
		cout << "Your file language is ambiguous" << endl;
	}
}	
