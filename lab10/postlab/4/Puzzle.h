// Taylor Rongaus
// Lab 6, due 3/4/16
// puzzle.h

#ifndef PUZZLE_H
#define PUZZLE_H
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <cstring>
#include <set>
using namespace std;

template<typename T>
class Puzzle {
	public:
		Puzzle(string);						// non-default constructor
		void printPuzzle();				// print the puzzle
		int gameWin();					// determine if game has been won
		void vectorInit();					// 3D vector initialization
		void printSolver();				// print the solver
		int checkSurr(int, int, int);	// check surroundings of position
		void checkSolver();				// update solverVector
		void checkSingle();				// see if any positions only have 1 possibility
		void solver();						// solver function
	private:
		vector< vector<T> >puzzleBoard;					// vector for puzzle board
		vector< vector< set <T> > >solverVector;	// vector for solver
};


// non-default constructor
template<typename T>
Puzzle<T>::Puzzle(string file) {
	string line;
	vector <int> tmp(9);
	int iRow = 0;
	ifstream myFile (file.c_str());
	if (myFile.is_open()) {
		while(myFile.good()) {
			puzzleBoard.push_back(tmp);
			for (int iCol = 0; iCol < 9; iCol++) {
				myFile >> puzzleBoard[iRow][iCol];
			}
			iRow++;
		}
	}
}

// printPuzzle function
template<typename T>
void Puzzle<T>::printPuzzle() {
	for (int j = 0; j < puzzleBoard.size()-1; j++) {
		if (j%3 == 0) {
			cout << "-------------------------------" << endl;
		}
		for (int k = 0; k <= puzzleBoard[j].size(); k++) {
			if (k == puzzleBoard[j].size()) {
				cout << "|";
			}
			else {
				if (k%3 == 0) {
					cout << "|";
				}
				cout << " " << puzzleBoard[j][k] << ' ';
			}
		}
		cout << endl;
	}
	cout << "-------------------------------" << endl;
}

// determine if game has been won
template<typename T>
int Puzzle<T>::gameWin() {
	int win = 0;
	int numCount = 0;
	// check to see if board is correctly filled
	for (int i = 0; i < puzzleBoard.size(); i++) {
		for (int j = 0; j<puzzleBoard[j].size(); j++) {
			if (puzzleBoard[i][j] != 0) {
				numCount++;
			}
		}
	}
	if (numCount == 81) {
		win = 1;
	}
	return win;
}

// initialize 3D vector
template<typename T>
void Puzzle<T>::vectorInit() {
	set<T> v;
	vector<set<T> > v2(9,v);
	vector<vector<set<T> > > v3(9,v2);
	solverVector=v3;
	// fill the vector with zeros
	for (int i=0;i<9;i++) {
		for (int j=0;j<9;j++) {
			for (int k=1;k<10;k++) {
				solverVector[i][j].insert(k);
			}
		}
	}
}

// print the solver
template<typename T>
void Puzzle<T>::printSolver() {
	for (int i=0;i<9;i++) {
		for (int j=0;j<9;j++) {
			cout << "Row: " << i+1 << endl;
			cout << "Column: " << j+1 << endl;
			for (set<int>::iterator k=solverVector[i][j].begin(); k!=solverVector[i][j].end(); ++k){
				cout << "Set: " << *k << endl;
			}
			cout << endl;
		}
	}
}

// check surroundings of position chosen
template<typename T>
int Puzzle<T>::checkSurr(int row, int col, int num){
	// define local variables
	int canPlace = 1;
	int up_row, left_col;

	// check puzzleBoard to see if cannot place number
	for (int i = 0; i < 9; i++) {
		if (puzzleBoard[row-1][i] == num) {
			canPlace = 0;	// if num already exists, cannot place here
		}
		else if (puzzleBoard[i][col-1] == num) {
			canPlace = 0;	// if num already exists, cannot place here
		}
		else if (puzzleBoard[row-1][col-1] != 0) {
			canPlace = 0;	// if board position not a zero, cannot place here
		}
	}

	//set location to top left position of the chosen 3x3 grid
	// set row to "highest up" row in grid
	if (row <= 3) {
		up_row = 0;
	}
	else if (row <= 6 && row > 3) {
		up_row = 3;
	}
	else if (row <= 9 && row > 6) {
		up_row = 6;
	}
	// set col to "most left" column in grid
	if (col <= 3) {
		left_col = 0;
	}
	else if (col <= 6 && col > 3) {
		left_col = 3;
	}
	else if (col <= 9 && col > 6) {
		left_col = 6;
	}
	
	// check 3x3 grid to see if any number matches position chosen
	for (int j = up_row; j < up_row+3; j++) {
		for (int k = left_col; k < left_col+3; k++) {
			if (puzzleBoard[j][k] == num) {
				canPlace = 0;
			}
		}
	}
	return canPlace;
}

// call checkSurr to update solverVector
template<typename T>
void Puzzle<T>::checkSolver() {
	for (int i=1;i<10;i++) {
		for (int j=1;j<10;j++) {
			for (set<int>::iterator k=solverVector[i-1][j-1].begin(); k!=solverVector[i-1][j-1].end(); ){
				if (checkSurr(i,j,*k) == 0) {
					k = solverVector[i-1][j-1].erase(k);
				}
				else {
					++k;
				}
			}
		}
	}
}

// check to see if any positions only have 1 possibility
template<typename T>
void Puzzle<T>::checkSingle() {
	// count stores the number of possibilities for a position
	// num stores the possible number for a position
	for (int i=0;i<9;i++) {
		for (int j=0;j<9;j++) {
			if (solverVector[i][j].size() == 1) {
				puzzleBoard[i][j] = *(solverVector[i][j].begin());
			}
		}
	}
}

// solver function
template<typename T>
void Puzzle<T>::solver() {
	// call the 3D vector initializer
	vectorInit();
	int solved = 0;
	// fill in 0s where values already exist
	for (int i=0;i<9;i++) {
		for (int j=0;j<9;j++) {
			for (set<int>::iterator k=solverVector[i][j].begin(); k!=solverVector[i][j].end(); ){
				if (puzzleBoard[i][j] != 0) {
					k = solverVector[i][j].erase(k);
				}
				else {
					++k;
				}
			}
		}
	}
	// loop through solver functions until puzzle fully solved
	while (!solved) {
		checkSolver();		// update solverVector
		checkSingle();		// check to see if any positions only have 1 possibility
		solved = gameWin();	// check to see if puzzleBoard is filled
	}
	cout << "Solved Puzzle: " << endl;
	printPuzzle();				// print puzzleBoard when solving completed
}

#endif
