// detect.cpp
// due 4/18/16
// Author: Taylor Rongaus
// This program will detect/process similar documents
// ========================================

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstring>
#include <set>
#include <map>
#include <algorithm>
using namespace std;

// function prototype declarations
// ========================================

set<string> fileOpen1(string file);
map<string, int> fileOpen2(string file);


// main execution
// ========================================

int main() {
	// read in the names of the files
	string file1, file2;
	cout << "What is the name of the first file? ";
	cin >> file1;
	cout << "What is the name of the second file? ";
	cin >> file2;

	// call function to open the file & display info
	set<string> set1 = fileOpen1(file1);
	map<string, int> map1 = fileOpen2(file2);
	cout << "There are " << set1.size() << " unique words in the first file." << endl;
	cout << "The following is a list of unique words and counts in the second file:" << endl;
	for (map<string, int>::iterator it=map1.begin();it!=map1.end(); ++it){
		cout << it->first << " ------------> " << it->second << '\n';
	}
	
}


// functions
// ========================================

// open the file, return a set of unique words only
set<string> fileOpen1(string file) {
	string tmp;
	ifstream myFile (file.c_str());
	set<string> set1;
	if (myFile.is_open()) {
		while(myFile.good()) {
				myFile >> tmp;
				// go from upper case to lower case
				transform(tmp.begin(), tmp.end(), tmp.begin(), :: tolower);
				// remove punctuation
				for (int i = 0; i < tmp.size(); i++) {
					if (ispunct(tmp[i])) {
						tmp.erase(i--, 1);
					}
				}
				set1.insert(tmp);
		}
	}
	return set1;
}

// open the file, return a map of unique words and counts
map<string, int> fileOpen2(string file) {
	string tmp;
	ifstream myFile (file.c_str());
	map<string, int> map1;
	int value = 1;
	if (myFile.is_open()) {
		while(myFile.good()) {
				myFile >> tmp;
				// go from upper case to lower case
				transform(tmp.begin(), tmp.end(), tmp.begin(), :: tolower);
				// remove punctuation
				for (int i = 0; i < tmp.size(); i++) {
					if (ispunct(tmp[i])) {
						tmp.erase(i--, 1);
					}
				}
				map1[tmp]++;
		}
	}
	return map1;
}

