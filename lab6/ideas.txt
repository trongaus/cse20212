3D possibilities vector: [x][y][0-9] (where 3rd element is of size 9 and 
can either be a 0 or 1)
	ex: p[2][3][4]  - at position (2,3), the option for 4 can either be 
	a 0 (meaning you cannot place 4 there) or 1 (meaning that you can 
	place a 4 there)

1st loop: determine 1s and 0s for the 3rd vector element for every 
position of the board

2nd loop: determine if any of the 3rd vectors has only one 1. If so, 
fill the corresponding position in the puzzleBoard w/ the number of the 
element w/ the 1

~ repeat this process until complete

~ also need to implement the singleton algorithm
