// Taylor Rongaus
// Lab 6, due 3/4/16
// puzzle.h

#ifndef PUZZLE_H
#define PUZZLE_H
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <cstring>
using namespace std;

template<typename T>
class Puzzle {
	public:
		Puzzle(string);						// non-default constructor
		void printPuzzle();				// print the puzzle
		int gameWin();					// determine if game has been won
		void vectorInit();					// 3D vector initialization
		void printSolver();				// print the solver
		int checkSurr(int, int, int);	// check surroundings of position
		void checkSolver();				// update solverVector
		void checkSingle();				// see if any positions only have 1 possibility
		void singletonRow();			// check rows for singletons
		void singletonCol();			// check columns for singletons
		void solver();						// solver function
	private:
		vector< vector<T> >puzzleBoard;					// vector for puzzle board
		vector< vector< vector <T> > >solverVector;	// vector for solver
};


// non-default constructor
template<typename T>
Puzzle<T>::Puzzle(string file) {
	string line;
	vector <int> tmp(9);
	int iRow = 0;
	ifstream myFile (file.c_str());
	if (myFile.is_open()) {
		while(myFile.good()) {
			puzzleBoard.push_back(tmp);
			for (int iCol = 0; iCol < 9; iCol++) {
				myFile >> puzzleBoard[iRow][iCol];
			}
			iRow++;
		}
	}
}

// printPuzzle function
template<typename T>
void Puzzle<T>::printPuzzle() {
	for (int j = 0; j < puzzleBoard.size()-1; j++) {
		if (j%3 == 0) {
			cout << "-------------------------------" << endl;
		}
		for (int k = 0; k <= puzzleBoard[j].size(); k++) {
			if (k == puzzleBoard[j].size()) {
				cout << "|";
			}
			else {
				if (k%3 == 0) {
					cout << "|";
				}
				cout << " " << puzzleBoard[j][k] << ' ';
			}
		}
		cout << endl;
	}
	cout << "-------------------------------" << endl;
}

// determine if game has been won
template<typename T>
int Puzzle<T>::gameWin() {
	int win = 0;
	int numCount = 0;
	// check to see if board is correctly filled
	for (int i = 0; i < puzzleBoard.size(); i++) {
		for (int j = 0; j<puzzleBoard[j].size(); j++) {
			if (puzzleBoard[i][j] != 0) {
				numCount++;
			}
		}
	}
	if (numCount == 81) {
		win = 1;
	}
	return win;
}

// initialize 3D vector
template<typename T>
void Puzzle<T>::vectorInit() {
	vector<T> v(9);
	vector<vector<T> > v2(9,v);
	vector<vector<vector<T> > > v3(9,v2);
	solverVector=v3;
	// fill the vector with zeros
	for (int i=0;i<9;i++) {
		for (int j=0;j<9;j++) {
			for (int k=0;k<9;k++) {
				solverVector[i][j][k] = 1;
			}
		}
	}
}

// print the solver
template<typename T>
void Puzzle<T>::printSolver() {
	for (int i=0;i<9;i++) {
		for (int j=0;j<9;j++) {
			cout << "Row: " << i+1 << endl;
			cout << "Column: " << j+1 << endl;
			for (int k=0;k<9;k++) {
				cout << "Value of position " << k+1 << " is " << solverVector[i][j][k] << endl;
			}
			cout << endl;
		}
	}
}

// check surroundings of position chosen
template<typename T>
int Puzzle<T>::checkSurr(int row, int col, int num){
	// define local variables
	int canPlace = 1;
	int up_row, left_col;

	// check puzzleBoard to see if cannot place number
	for (int i = 0; i < 9; i++) {
		if (puzzleBoard[row-1][i] == num) {
			canPlace = 0;	// if num already exists, cannot place here
		}
		else if (puzzleBoard[i][col-1] == num) {
			canPlace = 0;	// if num already exists, cannot place here
		}
		else if (puzzleBoard[row-1][col-1] != 0) {
			canPlace = 0;	// if board position not a zero, cannot place here
		}
	}

	//set location to top left position of the chosen 3x3 grid
	// set row to "highest up" row in grid
	if (row <= 3) {
		up_row = 0;
	}
	else if (row <= 6 && row > 3) {
		up_row = 3;
	}
	else if (row <= 9 && row > 6) {
		up_row = 6;
	}
	// set col to "most left" column in grid
	if (col <= 3) {
		left_col = 0;
	}
	else if (col <= 6 && col > 3) {
		left_col = 3;
	}
	else if (col <= 9 && col > 6) {
		left_col = 6;
	}
	
	// check 3x3 grid to see if any number matches position chosen
	for (int j = up_row; j < up_row+3; j++) {
		for (int k = left_col; k < left_col+3; k++) {
			if (puzzleBoard[j][k] == num) {
				canPlace = 0;
			}
		}
	}
	return canPlace;
}

// call checkSurr to update solverVector
template<typename T>
void Puzzle<T>::checkSolver() {
	for (int i=1;i<10;i++) {
		for (int j=1;j<10;j++) {
			for (int k=1;k<10;k++) {
				if (checkSurr(i,j,k) == 0) {
					solverVector[i-1][j-1][k-1] = 0;
				}
			}
		}
	}
}

// check to see if any positions only have 1 possibility
template<typename T>
void Puzzle<T>::checkSingle() {
	// count stores the number of possibilities for a position
	// num stores the possible number for a position
	int count = 0, num = 0;
	for (int i=0;i<9;i++) {
		for (int j=0;j<9;j++) {
			for (int k=0;k<9;k++) {
				if (solverVector[i][j][k] != 0) {
					// if solverVector indicates possibility, increment count &
					// set num to the possible number
					count++;
					num=(k+1);
				}
			}
			if (count == 1) {
				// if only one possibility, set puzzleBoard to that number
				puzzleBoard[i][j] = num;
			}
			// reset count
			count = 0;
		}
	}
}

// check rows for singletons
template<typename T>
void Puzzle<T>::singletonRow() {
	// initialize row and temporary vectors
	vector <T> row;
	vector<T> temp;
	for (int i = 0; i < 9; i++) {
		temp.push_back(0);
	}
	// if solverVector indicates possibility, increment row in that position
	for (int i = 0; i < 9; i++) {
		row = temp;
		for (int j = 0; j < 9; j++) {
			for (int k = 0; k < 9; k++) {
				if (solverVector[i][j][k] == 1) {
					row[k]++;
				}
			}
		}
		// if number is only possible once in that row, place it on puzzleBoard
		for (int m = 0; m < 9; m++) {
			for (int n = 0; n < 9; n++) {
				if ((checkSurr(i+1,m+1,n+1)) && (row[n] == 1)) {
					puzzleBoard[i][m] = (n+1);
				}
			}
		}
	}
}

// check columns for singletons
template<typename T>
void Puzzle<T>::singletonCol() {
	// initialize col and temporary vectors
	vector <T> col;
	vector<T> temp;
	for (int i = 0; i < 9; i++) {
		temp.push_back(0);
	}
	// if solverVector indicates possibility, increment col in that position
	for (int i = 0; i < 9; i++) {
		col = temp;
		for (int j = 0; j < 9; j++) {
			for (int k = 0; k < 9; k++) {
				if (solverVector[i][j][k] == 1) {
					col[k]++;
				}
			}
		}
		// if number is only possible once in that col, place it on puzzleBoard
		for (int m = 0; m < 9; m++) {
			for (int n = 0; n < 9; n++) {
				if ((checkSurr(i+1,m+1,n+1)) && (col[n] == 1)) {
					puzzleBoard[m][i] = (n+1);
				}
			}
		}
	}
}

// solver function
template<typename T>
void Puzzle<T>::solver() {
	// call the 3D vector initializer
	vectorInit();
	int solved = 0;
	// fill in 0s where values already exist
	for (int i=0;i<9;i++) {
		for (int j=0;j<9;j++) {
			if (puzzleBoard[i][j] != 0) {
				solverVector[i][j][(puzzleBoard[i][j])-1] =0;
			}
		}
	}
	// loop through solver functions until puzzle fully solved
	while (!solved) {
		checkSolver();		// update solverVector
		checkSingle();		// check to see if any positions only have 1 possibility
		singletonRow();		// check rows for singletons
		singletonCol();		// check columns for singletons
		solved = gameWin();	// check to see if puzzleBoard is filled
	}
	cout << "Solved Puzzle: " << endl;
	printPuzzle();				// print puzzleBoard when solving completed
}

#endif
