#include "Puzzle.h"
using namespace std;

int main() {
	string file;
	cout << "What is the name of the file you'd like to import? ";
	cin >> file;
	Puzzle<int> p (file);
	p.playGame();
}

