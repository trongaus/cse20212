// Taylor Rongaus
// Lab 5, due 2/22/16
// puzzle.h

#ifndef PUZZLE
#define PUZZLE
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <cstring>
using namespace std;

template<typename T>
class Puzzle {
	public:
		Puzzle(string);											// non-default constructor
		void print();											// print function
		void playGame();									// gameplay function
	private:
		vector< vector<T> >puzzleBoard;				// vector for puzzle board
};


// non-default constructor
template<typename T>
Puzzle<T>::Puzzle(string file) {
	string line;
	vector <int> tmp(9);
	int iRow = 0;
	ifstream myFile (file.c_str());
	if (myFile.is_open()) {
		while(myFile.good()) {
			puzzleBoard.push_back(tmp);
			for (int iCol = 0; iCol < 9; iCol++) {
				myFile >> puzzleBoard[iRow][iCol];
			}
			iRow++;
		}
	}
}

// print function
template<typename T>
void Puzzle<T>::print() {
	for (int j = 0; j < puzzleBoard.size()-1; j++) {
		if (j%3 == 0) {
			cout << "-------------------------------" << endl;
		}
		for (int k = 0; k <= puzzleBoard[j].size(); k++) {
			if (k == puzzleBoard[j].size()) {
				cout << "|";
			}
			else {
				if (k%3 == 0) {
					cout << "|";
				}
				cout << " " << puzzleBoard[j][k] << ' ';
			}
		}
		cout << endl;
	}
	cout << "-------------------------------" << endl;
}

// gameplay function
template<typename T>
void Puzzle<T>::playGame() {
	// set check to determine if board has been correctly filled
	int solve = 0;
	cout << endl;
	cout << "----------- SUDOKU -----------" << endl;

	// while loop to continue gameplay until puzzle has been solved
	while (solve == 0) {
	
		// print board
		cout << endl;
		print();
		cout << endl;
	
		// initialize variables
		int myRow = 0, myCol = 0, myNum = 0, numCount = 0, validChoice = 1;
	
		// get user row/ column to change, ensure if within proper range
		while ((myRow < 1) || (myRow > 9)) {
			cout << "Which row do you want to change? ";
			cin >> myRow;
			if ((myRow <1) || (myRow > 9)) {
				cout << "Error: please enter a row value between 1 and 9." << endl;
			}
		}
		while ((myCol < 1) || (myCol > 9)) {
			cout << "Which column do you want to change? ";
			cin >> myCol;
			if ((myCol <1) || (myCol > 9)) {
				cout << "Error: please enter a column value between 1 and 9." << endl;
			}
		}
	
		// get user's number choice, ensure if within proper range
		while ((myNum < 1) || (myNum > 9)) {
			cout << "What number would you like to enter? ";
			cin >> myNum;
			// check that number choice is a valid 1-9 number
			if ((myNum < 1) || (myNum > 9)) {
				cout << "Error: please enter a column value between 1 and 9." << endl;
			}
		}
		
		// check that number isn't already in row
		for (int iCol = 0; iCol < 9; iCol++) {
			if (puzzleBoard[myRow-1][iCol] == myNum) {
				cout << "Error: that number already exists in that row. Please enter a valid number" << endl;
				myNum = 0;
				goto endchecks;
			}
		}
		
		// check that number isn't already in column
		for (int iRow = 0; iRow < 9; iRow++) {
			if (puzzleBoard[iRow][myCol-1] == myNum) {
				cout << "Error: that number already exists in that column. Please enter a valid number" << endl;
				myNum = 0;
				goto endchecks;
			}
		}
		
		// check if number is already in a 3x3 box
		switch (myRow) {
			case 1:
			case 2:
			case 3:
				// box 1
				if (myCol >=1 && myCol <=3) {
					for (int iRow=0;iRow<3;iRow++) {
						for (int iCol=0;iCol<3;iCol++) {
							if (puzzleBoard[iRow][iCol] == myNum) {
								myNum = 0;
								cout << "Error: that number already exists in that box. Please enter a valid number." << endl;
								goto endchecks;
							}
						}
					}
				}
				// box 2
				else if (myCol >=4 && myCol <=6) {
					for (int iRow=0;iRow<3;iRow++) {
						for (int iCol=3;iCol<6;iCol++) {
							if (puzzleBoard[iRow][iCol] == myNum) {
								myNum = 0;
								cout << "Error: that number already exists in that box. Please enter a valid number." << endl;
								goto endchecks;
							}
						}
					}
				}
				// box 3
				else if (myCol >= 7 && myCol <= 9) {
					for (int iRow=0;iRow<3;iRow++) {
						for (int iCol=6;iCol<9;iCol++) {
							if (puzzleBoard[iRow][iCol] == myNum) {
								myNum = 0;
								cout << "Error: that number already exists in that box. Please enter a valid number." << endl;
								goto endchecks;
							}
						}
					}
				}
				break;
			case 4:
			case 5:
			case 6:
				// box 4
				if (myCol >=1 && myCol <=3) {
					for (int iRow=3;iRow<6;iRow++) {
						for (int iCol=0;iCol<3;iCol++) {
							if (puzzleBoard[iRow][iCol] == myNum) {
								myNum = 0;
								cout << "Error: that number already exists in that box. Please enter a valid number." << endl;
								goto endchecks;
							}
						}
					}
				}
				// box 5
				else if (myCol >=4 && myCol <=6) {
					for (int iRow=3;iRow<6;iRow++) {
						for (int iCol=3;iCol<6;iCol++) {
							if (puzzleBoard[iRow][iCol] == myNum) {
								myNum = 0;
								cout << "Error: that number already exists in that box. Please enter a valid number." << endl;
								goto endchecks;
							}
						}
					}
				}
				// box 6
				else if (myCol >= 7 && myCol <= 9) {
					for (int iRow=3;iRow<6;iRow++) {
						for (int iCol=6;iCol<9;iCol++) {
							if (puzzleBoard[iRow][iCol] == myNum) {
								myNum = 0;
								cout << "Error: that number already exists in that box. Please enter a valid number." << endl;
								goto endchecks;
							}
						}
					}
				}
				break;
			case 7:
			case 8:
			case 9:
				// box 7
				if (myCol >=1 && myCol <=3) {
					for (int iRow=6;iRow<9;iRow++) {
						for (int iCol=0;iCol<3;iCol++) {
							if (puzzleBoard[iRow][iCol] == myNum) {
								myNum = 0;
								cout << "Error: that number already exists in that box. Please enter a valid number." << endl;
								goto endchecks;
							}
						}
					}
				}
				// box 8
				else if (myCol >=4 && myCol <=6) {
					for (int iRow=6;iRow<9;iRow++) {
						for (int iCol=3;iCol<6;iCol++) {
							if (puzzleBoard[iRow][iCol] == myNum) {
								myNum = 0;
								cout << "Error: that number already exists in that box. Please enter a valid number." << endl;
								goto endchecks;
							}
						}
					}
				}
				// box 9
				else if (myCol >= 7 && myCol <= 9) {
					for (int iRow=6;iRow<9;iRow++) {
						for (int iCol=6;iCol<9;iCol++) {
							if (puzzleBoard[iRow][iCol] == myNum) {
								myNum = 0;
								cout << "Error: that number already exists in that box. Please enter a valid number." << endl;
								goto endchecks;
							}
						}
					}
				}
				break;
		}
		// box error goto
		endchecks:
 
 		// update board position w/ user number choice
		puzzleBoard[myRow-1][myCol-1] = myNum;
		
		// check to see if board is correctly filled
		for (int j = 0; j < puzzleBoard.size(); j++) {
			for (int k = 0; k<puzzleBoard[j].size(); k++) {
				if (puzzleBoard[j][k] != 0) {
					numCount = numCount + 1;
					if (numCount == 81) {
						solve = 1;
					}
				}
			}
		}
		
	} // end while loop to continue gameplay until puzzle has been solved
	cout << "\nCongrats! You win!" << endl << endl;
}


#endif
